package fr.si2.queignec;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public static Context appContext;
    private ArrayList<TodoItem> items;
    private RecyclerView recycler;
    private LinearLayoutManager manager;
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appContext = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(), AddItemActivity.class);
                startActivityForResult(intent,  AddItemActivity.CODE_ACTIVITE);
            }
        });
        Log.i("INIT", "Fin initialisation composantes");

        // Test d'ajout d'un item
//        TodoItem item = new TodoItem(TodoItem.Tags.Important, "Réviser ses cours");
//        TodoDbHelper.addItem(item, getBaseContext());
//        item = new TodoItem(TodoItem.Tags.Normal, "Acheter du pain");
//        TodoDbHelper.addItem(item, getBaseContext());

        // On récupère les items
        items = TodoDbHelper.getItems(getBaseContext());
        Log.i("INIT", "Fin initialisation items");

        // On initialise le RecyclerView
        recycler = (RecyclerView) findViewById(R.id.recycler);
        manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);

        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);

        setRecyclerViewItemTouchListener();
        Log.i("INIT", "Fin initialisation recycler");
       // if (getIntent().hasExtra("ouvertParNotifImp"))
        createNotif(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_debug) {
            Intent dbmanager = new Intent(this,AndroidDatabaseManager.class);
            startActivity(dbmanager);
            return true;
        }
        else if (id==R.id.delAllItems) {
            TodoDbHelper.supprimerItems(appContext);
            items.clear();
            adapter.notifyDataSetChanged();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source, RecyclerView.ViewHolder target) {
                adapter.moveItem(source.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                final TodoItem item = items.get(position);

                switch(swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        Intent intent=new Intent(getApplicationContext(), ModifyItemActivity.class);
                        intent.putExtra("item", item);
                        startActivityForResult(intent, ModifyItemActivity.CODE_ACTIVITE);
                        break;
                    case ItemTouchHelper.LEFT:
                        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.appContext);
                        builder.setTitle(R.string.deleteTitle);
                        builder.setMessage(getString(R.string.deleteMsg)+" "+item.getLabel());
                        builder.setPositiveButton(R.string.oui, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int indice = items.indexOf(item);
                                TodoDbHelper.supprimerItem(MainActivity.appContext, item);
                                items.remove(item);
                                adapter.notifyItemRemoved(indice);
                            }
                        });
                        builder.setNegativeButton(R.string.non, null);
                        AlertDialog dialog=builder.create();
                        dialog.show();
                        break;
                }

                recycler.getAdapter().notifyItemChanged(position);
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recycler);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==AddItemActivity.CODE_ACTIVITE && resultCode==RESULT_OK) {
            TodoItem newItem=TodoDbHelper.dernierItemCree(getBaseContext());
            items.add(newItem);
            newItem.setIndiceAfficheur(items.size()-1);
            TodoDbHelper.majIndiceAff(getBaseContext(), newItem);
            adapter.notifyItemInserted(items.size()-1);
            createNotif(1);
        } else if (requestCode==ModifyItemActivity.CODE_ACTIVITE && resultCode==RESULT_OK) {
            TodoItem item=(TodoItem)data.getSerializableExtra("item");
            items.remove(item.getIndiceAfficheur());
            items.add(item.getIndiceAfficheur(), item);
            adapter.notifyItemChanged(item.getIndiceAfficheur());
            createNotif(1);
        }
    }

    private void createNotif(int idAppel) {
        NotificationManager manager=(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent=new Intent(this, MainActivity.class);
        intent.putExtra("ouvertParNotif", 1);
        PendingIntent pendingIntent=PendingIntent.getActivity(this, 2, intent, 0);
        AlarmManager alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent notifAlarm=new Intent(this, NotifReceiver.class);
        int nbImportant=0;
        int nonFait=0;
        for(TodoItem item : items) {
            if (item.getTag().equals(TodoItem.Tags.Important) && item.isDone()==false)
                nbImportant++;
            if (item.isDone()==false) {
                nonFait++;
                Notification notifItem = new Notification.Builder(this)
                        .setContentTitle("TodoItem "+item.getLabel()).setContentText(getString(R.string.timeout)).setSmallIcon(R.drawable.ic_notif).setWhen(System.currentTimeMillis()+3600000).setContentIntent(pendingIntent).setAutoCancel(true).build();
                notifAlarm.putExtra("notif", notifItem);
                notifAlarm.putExtra("id", item.getId()+2);
                PendingIntent pendingIntentItem=PendingIntent.getBroadcast(this, 0, notifAlarm, 0);
                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(item.getDateMilisecond()-86400000);
                if (System.currentTimeMillis()<calendar.getTimeInMillis())
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentItem);
                else if (System.currentTimeMillis()<=item.getDateMilisecond()+86399999)
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntentItem);
            }
        }
        if (nbImportant>=5 && idAppel==1) {
            Notification notif = new Notification.Builder(this)
                .setContentTitle("TodoItem").setContentText(getString(R.string.tropImportants)).setSmallIcon(R.drawable.ic_notif).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).setAutoCancel(true).build();
            manager.notify(0, notif);
        }
        else if (idAppel==0) {
            Notification notif = new Notification.Builder(this)
                    .setContentTitle("TodoItem").setContentText(getString(R.string.nbNonFait)+" "+nonFait).setSmallIcon(R.drawable.ic_notif).setWhen(System.currentTimeMillis()+3600000).setContentIntent(pendingIntent).setAutoCancel(true).build();
            notifAlarm.putExtra("notif", notif);
            notifAlarm.putExtra("id", 1);
            PendingIntent pendingIntent2=PendingIntent.getBroadcast(this, 0, notifAlarm, 0);
            Calendar calendar=Calendar.getInstance();
            calendar.setTimeInMillis(calendar.getTimeInMillis()+3600000);
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent2);

        }
    }
}
