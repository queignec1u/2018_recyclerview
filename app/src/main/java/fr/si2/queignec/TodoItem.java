package fr.si2.queignec;

import java.io.Serializable;

/**
 * Created by phil on 06/02/17.
 */

public class TodoItem implements Serializable {
    public enum Tags {
        Faible("Faible"), Normal("Normal"), Important("Important");

        private String desc;
        Tags(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    private String label;
    private Tags tag;
    private boolean done;
    private int id;
    private int indiceAfficheur;
    private long dateMilisecond;

    public TodoItem(Tags tag, String label, long dateMilisecond) {
        this.tag = tag;
        this.label = label;
        this.done = false;
        this.id = 0;
        this.indiceAfficheur=0;
        this.dateMilisecond=dateMilisecond;

    }

    public TodoItem(String label, Tags tag, boolean done, int id, int indiceAfficheur, long dateMilisecond) {
        this.label = label;
        this.tag = tag;
        this.done = done;
        this.id = id;
        this.indiceAfficheur=indiceAfficheur;
        this.dateMilisecond=dateMilisecond;
    }

    public static Tags getTagFor(String desc) {
        for (Tags tag : Tags.values()) {
            if (desc.compareTo(tag.getDesc()) == 0)
                return tag;
        }

        return Tags.Faible;
    }

    public String getLabel() {
        return label;
    }

    public Tags getTag() {
        return tag;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndiceAfficheur() {
        return indiceAfficheur;
    }

    public void setIndiceAfficheur(int indiceAfficcheur) {
        this.indiceAfficheur = indiceAfficcheur;
    }

    public long getDateMilisecond() {
        return dateMilisecond;
    }

    public void setDateMilisecond(long dateMilisecond) {
        this.dateMilisecond = dateMilisecond;
    }
}
