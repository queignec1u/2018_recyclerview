package fr.si2.queignec;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.MatrixCursor;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by phil on 11/02/17.
 */

public class TodoDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "todo.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TodoContract.TodoEntry.TABLE_NAME + " (" +
                    TodoContract.TodoEntry._ID + " INTEGER PRIMARY KEY," +
                    TodoContract.TodoEntry.COLUMN_NAME_LABEL + " TEXT," +
                    TodoContract.TodoEntry.COLUMN_NAME_TAG + " TEXT,"  +
                    TodoContract.TodoEntry.COLUMN_NAME_DONE +  " INTEGER," +
                    TodoContract.TodoEntry.COLUMN_NAME_INDICE+ " INTEGER,"+
                    TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI+" INTEGER)";

    public TodoDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Rien pour le moment
    }

    static ArrayList<TodoItem> getItems(Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Création de la projection souhaitée
        String[] projection = {
                TodoContract.TodoEntry._ID,
                TodoContract.TodoEntry.COLUMN_NAME_LABEL,
                TodoContract.TodoEntry.COLUMN_NAME_TAG,
                TodoContract.TodoEntry.COLUMN_NAME_DONE,
                TodoContract.TodoEntry.COLUMN_NAME_INDICE,
                TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI
        };

        // Requête
        Cursor cursor = db.query(
                TodoContract.TodoEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        // Exploitation des résultats
        ArrayList<TodoItem> items = new ArrayList<TodoItem>();

        while (cursor.moveToNext()) {
            String label = cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_LABEL));
            TodoItem.Tags tag = TodoItem.getTagFor(cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TAG)));
            boolean done = (cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DONE)) == 1);
            int id=cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry._ID));
            int indice=cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_INDICE));
            long dateExpi=cursor.getLong(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI));
            TodoItem item = new TodoItem(label, tag, done, id, indice, dateExpi);
            if (indice>=items.size())
                items.add(item);
            else
                items.add(indice, item);
        }

         // Ménage
        dbHelper.close();

        // Retourne le résultat
        return items;
    }

    static void addItem(TodoItem item, Context context) {
        TodoDbHelper dbHelper = new TodoDbHelper(context);

        // Récupération de la base
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        // Création de l'enregistrement
        ContentValues values = new ContentValues();
        values.put(TodoContract.TodoEntry.COLUMN_NAME_LABEL, item.getLabel());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_TAG, item.getTag().getDesc());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_DONE, item.isDone());
        values.put(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI, item.getDateMilisecond());
        // Enregistrement
        long newRowId = db.insert(TodoContract.TodoEntry.TABLE_NAME, null, values);

        // Mise à jour de l'id
        item.setId((int)newRowId);

        // Ménage
        dbHelper.close();
    }

    static void setDoneItem(Context context, TodoItem item, boolean done) {
        int doneBDD;
        if (done)
            doneBDD=1;
        else
            doneBDD=0;
        String req = "UPDATE "+TodoContract.TodoEntry.TABLE_NAME+" SET "+TodoContract.TodoEntry.COLUMN_NAME_DONE+" = "+doneBDD+
                " WHERE "+ TodoContract.TodoEntry._ID+" = "+item.getId();
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL(req);
        dbHelper.close();
    }

    static void supprimerItems(Context context) {
        String req = "DELETE FROM "+TodoContract.TodoEntry.TABLE_NAME;
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL(req);
        dbHelper.close();
    }

    static void supprimerItem(Context context, TodoItem item) {
        String req = "DELETE FROM "+TodoContract.TodoEntry.TABLE_NAME+" WHERE "+TodoContract.TodoEntry._ID+"="+item.getId();
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL(req);
        String req2 = "UPDATE "+TodoContract.TodoEntry.TABLE_NAME+" SET "+TodoContract.TodoEntry.COLUMN_NAME_INDICE+"="+TodoContract.TodoEntry.COLUMN_NAME_INDICE+"-1 WHERE "+ TodoContract.TodoEntry._ID+">"+item.getId();
        db.execSQL(req2);
        dbHelper.close();
    }

    static void majIndiceAff(Context context, TodoItem item) {
        String req = "UPDATE "+TodoContract.TodoEntry.TABLE_NAME+" SET "+ TodoContract.TodoEntry.COLUMN_NAME_INDICE+"="+item.getIndiceAfficheur()+" WHERE "+ TodoContract.TodoEntry._ID+"="+item.getId();
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL(req);
        dbHelper.close();
    }

    static void majItem(Context context, TodoItem item) {
        String req = "UPDATE "+TodoContract.TodoEntry.TABLE_NAME+" SET "+ TodoContract.TodoEntry.COLUMN_NAME_LABEL+"='"+item.getLabel()+"', "+ TodoContract.TodoEntry.COLUMN_NAME_TAG+"='"+item.getTag().getDesc()+"', "+ TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI+'='+item.getDateMilisecond()+" WHERE "+ TodoContract.TodoEntry._ID+"="+item.getId();
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.execSQL(req);
        dbHelper.close();
    }

    static TodoItem dernierItemCree(Context context) {
        String req="SELECT * FROM "+TodoContract.TodoEntry.TABLE_NAME+" WHERE "+TodoContract.TodoEntry._ID+"=(SELECT MAX("+TodoContract.TodoEntry._ID+") FROM "+TodoContract.TodoEntry.TABLE_NAME+")";
        TodoDbHelper dbHelper=new TodoDbHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor=db.rawQuery(req, new String[]{});
        cursor.moveToFirst();
        String label = cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_LABEL));
        TodoItem.Tags tag = TodoItem.getTagFor(cursor.getString(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TAG)));
        boolean done = (cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DONE)) == 1);
        int id=cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry._ID));
        int indice=cursor.getInt(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_INDICE));
        long dateExpi=cursor.getLong(cursor.getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DATE_EXPI));
        TodoItem item = new TodoItem(label, tag, done, id, indice, dateExpi);
        dbHelper.close();
        return item;
    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "message" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }
}
