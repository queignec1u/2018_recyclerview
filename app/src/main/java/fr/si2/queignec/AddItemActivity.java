package fr.si2.queignec;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.Calendar;

public class AddItemActivity extends AppCompatActivity {
    public static final int CODE_ACTIVITE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((DatePicker)findViewById(R.id.dateExpi)).setMinDate(System.currentTimeMillis()-1000);
        setSupportActionBar(toolbar);
    }

    public void valider(View v) {
        String label = ((EditText) findViewById(R.id.labelEdit)).getText().toString();
        RadioButton faible = (RadioButton) findViewById(R.id.radioFaible);
        RadioButton normal = (RadioButton) findViewById(R.id.radioNormal);
        RadioButton important = (RadioButton) findViewById(R.id.radioImportant);
        if (!label.equals("") && (faible.isChecked() || normal.isChecked() || important.isChecked())) {
            DatePicker date=(DatePicker)findViewById(R.id.dateExpi);
            TodoItem.Tags tag;
            if (faible.isChecked())
                tag = TodoItem.Tags.Faible;
            else if (normal.isChecked())
                tag = TodoItem.Tags.Normal;
            else
                tag = TodoItem.Tags.Important;
            Calendar c=Calendar.getInstance();
            c.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
            TodoItem item = new TodoItem(tag, label, c.getTimeInMillis());
            TodoDbHelper.addItem(item, this);
            setResult(RESULT_OK);
            finish();
        } else
            Toast.makeText(this, R.string.add_not_complete, Toast.LENGTH_SHORT).show();
    }

    public void annuler(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }
}
