package fr.si2.queignec;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by phil on 07/02/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder> {

    private ArrayList<TodoItem> items;

    public RecyclerAdapter(ArrayList<TodoItem> items) {
        this.items = items;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
    }

    public void moveItem(int posAv, int posAp) {
        TodoItem item=items.get(posAv);
        items.remove(item);
        items.add(posAp, item);
        for (TodoItem it : items) {
            it.setIndiceAfficheur(items.indexOf(it));
            TodoDbHelper.majIndiceAff(MainActivity.appContext, it);
        }
        notifyItemMoved(posAv, posAp);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class TodoHolder extends RecyclerView.ViewHolder {
        private Resources resources;
        private ImageView image;
        private SwitchCompat sw;
        private TextView label;
        private TodoItem item;

        public TodoItem getItem() {
            return item;
        }

        public TodoHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.imageView);
            sw = (SwitchCompat) itemView.findViewById(R.id.switch1);
            label = (TextView) itemView.findViewById(R.id.textView);
            resources = itemView.getResources();
        }

        public void bindTodo(final TodoItem todo) {
            this.item=todo;
            label.setText(todo.getLabel());
            sw.setChecked(todo.isDone());
            if (todo.isDone())
                itemView.setBackgroundColor(resources.getColor(R.color.fait));
            switch(todo.getTag()) {
                case Faible:
                    image.setBackgroundColor(resources.getColor(R.color.faible));
                    break;
                case Normal:
                    image.setBackgroundColor(resources.getColor(R.color.normal));
                    break;
                case Important:
                    image.setBackgroundColor(resources.getColor(R.color.important));
                    break;
            }
            sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    todo.setDone(isChecked);
                    TodoDbHelper.setDoneItem(MainActivity.appContext, todo, todo.isDone());
                    if (todo.isDone())
                        itemView.setBackgroundColor(resources.getColor(R.color.fait));
                    else
                        itemView.setBackgroundColor(Color.WHITE);
                }
            });
        }
    }
}
