package fr.si2.queignec;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.Calendar;

public class ModifyItemActivity extends AppCompatActivity {
    public static final int CODE_ACTIVITE=2;
    private TodoItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        item=(TodoItem)getIntent().getSerializableExtra("item");
        EditText editText=(EditText)findViewById(R.id.labelEdit);
        editText.setText(item.getLabel());
        String tag=item.getTag().getDesc();
        if (tag.equals("Faible"))
            ((RadioButton)findViewById(R.id.radioFaible)).setChecked(true);
        else if (tag.equals("Normal"))
            ((RadioButton)findViewById(R.id.radioNormal)).setChecked(true);
        else if (tag.equals("Important"))
            ((RadioButton)findViewById(R.id.radioImportant)).setChecked(true);
        Calendar calendar=Calendar.getInstance();
        DatePicker datePicker=(DatePicker)findViewById(R.id.dateExpi);
        if (calendar.getTimeInMillis()>item.getDateMilisecond())
            calendar.setTimeInMillis(item.getDateMilisecond());
        datePicker.setMinDate(calendar.getTimeInMillis()-1000);
        calendar.setTimeInMillis(item.getDateMilisecond());
        datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public void valider(View v) {
        String label = ((EditText) findViewById(R.id.labelEdit)).getText().toString();
        RadioButton faible = (RadioButton) findViewById(R.id.radioFaible);
        RadioButton normal = (RadioButton) findViewById(R.id.radioNormal);
        RadioButton important = (RadioButton) findViewById(R.id.radioImportant);
        if (!label.equals("")) {
            DatePicker date=(DatePicker)findViewById(R.id.dateExpi);
            TodoItem.Tags tag=item.getTag();
            if (faible.isChecked())
                tag= TodoItem.Tags.Faible;
            else if (normal.isChecked())
                tag= TodoItem.Tags.Normal;
            else
                tag= TodoItem.Tags.Important;
            Calendar c=Calendar.getInstance();
            c.set(date.getYear(), date.getMonth(), date.getDayOfMonth());
            TodoItem newItem=new TodoItem(label, tag, item.isDone(), item.getId(), item.getIndiceAfficheur(), c.getTimeInMillis());
            TodoDbHelper.majItem(getBaseContext(), newItem);
            Intent data=new Intent();
            data.putExtra("item", newItem);
            setResult(RESULT_OK, data);
            finish();
        } else
            Toast.makeText(this, R.string.label_vide, Toast.LENGTH_SHORT).show();
    }

    public void annuler(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }
}
